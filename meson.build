project('libgi', 'c',
	version: '0.1.0',
	license: 'MIT',
	meson_version: '>=1.0.0',
	default_options: [
		'c_std=c23',
		'warning_level=3',
		'werror=true',
	]
)

version = meson.project_version().split('-')[0]
version_major = version.split('.')[0]
version_minor = version.split('.')[1]
versioned_name = '@0@-@1@.@2@'.format(meson.project_name(), version_major, version_minor)

cc = meson.get_compiler('c')

add_project_arguments(cc.get_supported_arguments(
	[
		'-Wundef',
		'-Wunused',
		'-Wlogical-op',
		'-Wmissing-include-dirs',
		'-Wold-style-definition', # nop
		'-Wpointer-arith',
		'-Wstrict-prototypes',
		'-Wimplicit-fallthrough',
		'-Wmissing-prototypes',
		'-Wno-unknown-warning-option',
		'-Wno-unused-command-line-argument',
		'-Wvla',
	]),
	language: 'c',
)

features = {}

libgi_deps = []
libgi_src = []
libgi_inc = include_directories('include')

subdir('src')
subdir('tools')

subdir('include')

symbols_file = 'libgi.syms'
lib_gi = library(
	versioned_name, libgi_src,
	dependencies: libgi_deps,
	include_directories: [libgi_inc],
	install: true,
	link_args: '-Wl,--version-script,@0@/@1@'.format(meson.current_source_dir(), symbols_file),
	link_depends: symbols_file
)

libgi_vars = {}
foreach name, have : features
	libgi_vars += { 'have_' + name.underscorify(): have.to_string() }
endforeach

libgi = declare_dependency(
	link_with: lib_gi,
	dependencies: libgi_deps,
	include_directories: libgi_inc,
	variables: libgi_vars,
)

meson.override_dependency(versioned_name, libgi)

summary(features, bool_yn: true)

pkgconfig = import('pkgconfig')
pkgconfig.generate(
	lib_gi,
	name: versioned_name,
	description: 'library for game controllers',
	subdirs: versioned_name,
	url: 'https://gitlab.freedesktop.org/bl4ckb0ne/libgi',
	variables: libgi_vars,
)
