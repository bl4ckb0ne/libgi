/*
 * SPDX-FileCopyrightText: 2024 Collabora, Ltd.
 *
 * SPDX-License-Identifier: MIT
 */

#ifndef LIBGI_H
#define LIBGI_H

struct libgi_interface {
	int (*open)(const char *path, int flags, void *user_data);
	int (*close)(int fd, void *user_data);
};

struct libgi_ctx;

#endif
